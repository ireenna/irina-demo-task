define("UsrConcerts1Page", ["UsrConcertConstants"], function(concertConst) {
	return {
		entitySchemaName: "UsrConcerts",
		attributes: {
			"responseListEvents": {
                "dataValueType": Terrasoft.DataValueType.INTEGER,
                "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
            },
            "ActualActiveEverydayEvents": {
                "dataValueType": Terrasoft.DataValueType.INTEGER,
                "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
            },
			"MaxActiveEverydayEvents": {
                "dataValueType": Terrasoft.DataValueType.INTEGER,
                "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
            }
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "UsrConcertsFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "UsrConcerts"
				}
			},
			"UsrSchemaa775783eDetail8d6ff46f": {
				"schemaName": "UsrSchemaa775783eDetail",
				"entitySchemaName": "UsrConcertEvents",
				"filter": {
					"detailColumn": "UsrUsrConcerts",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			onEntityInitialized: function(){
                this.callParent(arguments);
                this.getPeriodicityActiveNumber();
				var maxActiveCount =this.Terrasoft.SysSettings.getCachedSysSetting("MaxActiveEverydayEvents"); //!
				this.set("MaxActiveEverydayEvents", maxActiveCount);
				this.set("Resources.Strings.CreatedOnMoreThanMaxActiveEvents",
						 "Cвободных концертных залов мало. Допускается не более "+ maxActiveCount+ " программ.");
            },
			getPeriodicityActiveNumber: function() {
                var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "UsrConcerts"
				});
				
				var periodicity = concertConst.PeriodicityEveryday.Id;
				esq.addAggregationSchemaColumn("UsrIsActive", Terrasoft.AggregationType.COUNT, "CountAll");
				var groupFilters = this.Ext.create("Terrasoft.FilterGroup");
				var filterId = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.NOT_EQUAL, "Id", this.get("Id"));
                var filterIsActive = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "UsrIsActive", true);
				 var filterPerodicity = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "UsrPeriodicity.Id", periodicity);
				groupFilters.addItem(filterId);
                groupFilters.logicalOperation = this.Terrasoft.LogicalOperatorType.AND;
                groupFilters.addItem(filterIsActive);
				groupFilters.logicalOperation = this.Terrasoft.LogicalOperatorType.AND;
                groupFilters.addItem(filterPerodicity);
				esq.filters.add(groupFilters);
				
				esq.getEntityCollection(function(response) {
				var isActiveCount = 0;
					if(response.success){
						isActiveCount = response.collection.collection.items[0].values.CountAll;
						this.set("ActualActiveEverydayEvents", isActiveCount);
					}else{
						this.showInformationDialog("Request error");
                        return;
					}
				}, this);
            },
			usrIsActiveWithPeriodicityValidator: function() {
				var invalidMessage = "";
				var maxActiveCount = this.get("MaxActiveEverydayEvents");
				var isActiveCount = this.get("ActualActiveEverydayEvents");
				var errorText = this.get("Resources.Strings.CreatedOnMoreThanMaxActiveEvents");
				
				if (this.get("UsrIsActive") && this.get("UsrPeriodicity").displayValue ==="Ежедневно") { //!
					isActiveCount++;
					if((isActiveCount) > maxActiveCount){
						invalidMessage = errorText;
						this.Terrasoft.showErrorMessage(invalidMessage);
					}
				}
				return {
					fullInvalidMessage: invalidMessage,
                    invalidMessage: invalidMessage
				};
            },
			setValidationConfig: function() {
                this.callParent(arguments);
                this.addColumnValidator("UsrIsActive", this.usrIsActiveWithPeriodicityValidator);
				this.addColumnValidator("UsrPeriodicity", this.usrIsActiveWithPeriodicityValidator);
            }
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "UsrName0d0f4e10-c106-4546-b637-6c2a6911cdbd",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrName"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "UsrNotesd80ed2d4-fb4d-4564-85dc-e503574b4c99",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrCode",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUPaa254d90-0549-437f-987e-d6bcb4f3f5e9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrResponsible",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUPd088a75d-6ceb-4b1b-b5cc-3cda42cd89cf",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrPeriodicity",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRINGca7abafa-8949-4e4b-bd18-65f7d2dd969b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrComments",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "BOOLEAN9ab60d8e-6e51-46bf-90af-7846259a84da",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrIsActive",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUPb7b40db9-3ed5-43d5-b48d-260a4d66b3f0",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "UsrCollective",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tabcdeeb769TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabcdeeb769TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "UsrSchemaa775783eDetail8d6ff46f",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabcdeeb769TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "remove",
				"name": "ESNTab"
			},
			{
				"operation": "remove",
				"name": "ESNFeedContainer"
			},
			{
				"operation": "remove",
				"name": "ESNFeed"
			}
		]/**SCHEMA_DIFF*/
	};
});
