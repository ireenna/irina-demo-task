 define("UsrConcertConstants", [], function() {

	var periodicityEveryday = {
		Id: "ad71c872-5f5e-4838-9c41-defe4ae7fdb7"
	};

	var periodicityEveryweek = {
		Id: "62d17e09-7838-4868-8c33-256b430d1f9e"
	};
	 
	 var periodicityEverymonth = {
		Id: "5730e37f-5635-4093-980d-f1bd19231cc8"
	};

	return {
		PeriodicityEveryday: periodicityEveryday,
		PeriodicityEveryweek: periodicityEveryweek,
		PeriodicityEverymonth: periodicityEverymonth
		
	};
});
